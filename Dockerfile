FROM nginx:1.20.1-alpine
COPY index.html image.png /usr/share/nginx/html/
RUN rm /etc/nginx/conf.d/*
ADD default.conf /etc/nginx/conf.d/
